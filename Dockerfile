FROM debian:bullseye-20220418
EXPOSE 5050
COPY  . /srv
WORKDIR /srv
RUN apt-get update
RUN apt-get upgrade -y
RUN apt install htop -y
RUN apt install python3 -y
RUN  apt install python3-pip -y
RUN pip3 install -r requirements.txt
CMD [ "gunicorn" , "--bind=0.0.0.0:9090", "hello:app" ]